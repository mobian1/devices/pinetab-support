pinetab-support (0.2.1) unstable; urgency=medium

  * Revert "d/control: depend on `u-boot-sunxi`"
    This reverts commit 74390f689692e58f6b4f0f617453e726c4ac83bf as Tow-Boot
    actually *does* include `crust`.

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Mon, 04 Apr 2022 19:07:41 +0200

pinetab-support (0.2.0) unstable; urgency=medium

  * d/control: depend on `u-boot-sunxi`
    Despite previously planning to switch to user-installed Tow-Boot as the
    bootloader for this device, we need to revert back to using
    `u-boot-sunxi` as the former lacks support for `crust`. This change will
    be reverted as soon as this issue is fixed.

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Mon, 04 Apr 2022 13:31:55 +0200

pinetab-support (0.1.9) unstable; urgency=medium

  * d/control: update dependencies.
    USB gadget config moved from mobian-tweaks-common to
    mobile-usb-networking, so make sure we don't lose it on the next
    upgrade. Moreover, we're moving to promoting Tow-Boot instead of
    embedding the bootloader in the image itself, so drop the dependency on
    u-boot-sunxi.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 02 Apr 2022 18:35:46 +0200

pinetab-support (0.1.8) unstable; urgency=medium

  * d/control: switch to 5.15 kernel
  * debian: add gbp.conf

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Thu, 02 Dec 2021 13:09:14 +0100

pinetab-support (0.1.7) unstable; urgency=medium

  * d/control: don't depend on arm-trusted-firmware

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 19 Jan 2021 18:24:35 +0100

pinetab-support (0.1.6) unstable; urgency=medium

  * d/control: drop dependency on u-boot-menu
  * d/control: switch to 5.10 kernel

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 06 Jan 2021 21:00:28 +0100

pinetab-support (0.1.5) unstable; urgency=medium

  * d/control: depend on mobian-pinetab-tweaks.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 04 Nov 2020 17:23:17 +0100

pinetab-support (0.1.4) unstable; urgency=medium

  * d/control: upgrade to kernel 5.9.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 03 Nov 2020 10:59:22 +0100

pinetab-support (0.1.3) unstable; urgency=medium

  * d/control: add dependency on OV5640 firmware for camera AF

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 06 Oct 2020 15:14:01 +0200

pinetab-support (0.1.2) unstable; urgency=medium

  * d/control: upgrade to 5.8 kernel

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 19 Sep 2020 10:29:41 +0200

pinetab-support (0.1.1) unstable; urgency=medium

  * remove pinetab-devtools package

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 05 Sep 2020 12:43:55 +0200

pinetab-support (0.1.0) unstable; urgency=medium

  * Initial release

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 02 Sep 2020 11:26:48 +0200
